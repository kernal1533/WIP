#!/usr/bin/python
"""Eve Online Salvage Solver"""
# Copyright (C) 2021  Tim Cumming
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Tim Cumming (Elusive One)
# Created: 19/04/21
# Modified: 08/10/21

import urllib3
import certifi
import json
import datetime
import dateutil.parser

from http.client import responses
from datetime import date
from common.classes import Item
from pathlib import Path

import logging
logger = logging.getLogger()

market_cache_file = Path('market.cache')
market_cache_json = Path('market.json')


def load_market_cache():
    # Lets try to load up previous market data from the ini file.
    if market_cache_json.exists():
        with open(market_cache_json, 'r') as f:
            json_data = json.load(f)  # Dictionary of market data with type ids as keys

        market_data = {}

        if json_data:
            for item in json_data:
                # No Data available from API (Too few or no sell/buy orders)
                # Check if item exists and we are doing an update
                market_data[int(item)] = Item(type_id=json_data[item]['type_id'],
                                              type_name=json_data[item]['type_name'],
                                              market=json_data[item]['market'],
                                              buy=json_data[item]['buy'],
                                              sell=json_data[item]['sell'],
                                              updated=dateutil.parser.isoparse(json_data[item]['updated']))
    else:
        market_data = {}

    return market_data


def save_market_cache(market_data):
    # Save the market_data to file
    def obj_to_dict(obj):
        # Need to convert the datetime objects to str
        if isinstance(obj, date):
            date_str = obj.isoformat()
            return date_str

        return obj.__dict__

    json_string = json.dumps(market_data, indent=4, default=obj_to_dict)

    with open(market_cache_json, 'w') as mc:
        mc.write(json_string + '\n')


def api_check(config, id_list, market_data):
    logger.debug('Running api_check...')
    logger.debug(id_list)
    cache_max_age = datetime.timedelta(0, config.cache_max_age)
    time_now = datetime.datetime.now(datetime.timezone.utc)  # Use to check if data age below (Needs to be TZ aware)

    id_list_checked = []
    for item_id in id_list:
        if item_id in market_data:
            # Compare time now vs cache data time to update if old
            cache_age = time_now - market_data[item_id].updated
            if cache_age > cache_max_age:
                id_list_checked.append(item_id)

            # Check market name vs cached data to update if incorrect
            if market_data[item_id].market != config.market_name:
                id_list_checked.append(item_id)

            # Add to log if above tests False
            if item_id not in id_list_checked:
                logger.debug(('%s found in market_data, will not be updated' % str(item_id)))
        else:
            id_list_checked.append(item_id)

    logger.debug(id_list_checked)

    cache_hit_percent = 100 * (len(id_list) - len(id_list_checked)) / len(id_list)
    logger.info(('Using %0.2f%% cached market_data...' % cache_hit_percent))

    item_list = []
    payload = {}

    if len(id_list_checked) > 0:
        for id_number in id_list_checked:
            item_list.append({'type_id': id_number})

        try:  # Try to connect to the API server
            http = urllib3.PoolManager(ca_certs=certifi.where())

            # payload = {"market_name": "jita", "items": [{"name": "Rifter"}, {"type_id": 34}]}
            payload["market_name"] = config.market_name.lower()  # Lowercase just in case (API requires)
            payload["items"] = item_list

            encoded_data = json.dumps(payload).encode('utf-8')

            resp = http.request(
                'POST',
                config.base_url,
                body=encoded_data,
                headers=config.headers)

            logger.debug(resp.geturl())
            logger.debug(resp.info())

            logger.debug((resp.status, responses[resp.status]))

            data = json.loads(resp.data.decode('utf-8'))['appraisal']

            for item in data['items']:
                # No Data available from API (Too few or no sell/buy orders)
                # API will return date of: 0001-01-01 00:00:00+00:00
                # Use time_now from earlier so there is a time in the cache age so we don't bash the API
                if item['prices']['updated'] == '0001-01-01T00:00:00Z':
                    logger.debug(('No data available for %s, using now as time' % str(item['typeID'])))
                    update_time = time_now
                else:
                    update_time = dateutil.parser.isoparse(item['prices']['updated'])

                # Check if item exists and we are doing an update
                if item['typeID'] in market_data:
                    market_data[int(item['typeID'])].market = config.market_name
                    market_data[int(item['typeID'])].buy = item['prices']['buy']['percentile']
                    market_data[int(item['typeID'])].sell = item['prices']['sell']['percentile']
                    market_data[int(item['typeID'])].updated = update_time
                else:
                    market_data[int(item['typeID'])] = Item(type_id=item['typeID'],
                                                            type_name=item['typeName'],
                                                            market=config.market_name,
                                                            buy=item['prices']['buy'][config.buy_data_field],
                                                            sell=item['prices']['sell'][config.sell_data_field],
                                                            updated=update_time)

        except urllib3.exceptions.ConnectionError as err:  # A Connection error occurred.
            error = ('Error Connecting to Evepraisal: ' + str(err.reason))  # Error String
            logger.info(error)
        except urllib3.exceptions.HTTPError as err:  # An HTTP error occurred.
            error = ('HTTP Error: %s\n' % (str(err)))  # Error String
            logger.info(error)
    else:
        logger.debug('API not used to update market_data...')

    return market_data
