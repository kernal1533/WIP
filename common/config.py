#!/usr/bin/python
"""Eve Online Salvage Solver"""
# Copyright (C) 2021  Tim Cumming
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Tim Cumming (Elusive One)
# Created: 03/04/21
# Modified: 08/10/21

import json
from pathlib import Path


class Config(object):
    def __init__(self, version, sql_file, missing_mats, min_runs, allow_config_override, sort_column,
                 market_name, buy_data_field, sell_data_field, cache_max_age):
        self.version = version

        # Location of our SQLite database
        self.sql_file = sql_file

        self.missing_mats = missing_mats
        self.min_runs = min_runs

        # allow_config_override = Y: missing_mats and min_runs will be automatically adjusted to return a result
        self.allow_config_override = allow_config_override

        # Output List Sorting Column: 0: Blueprint Name, 3: Max Runs Column, 5: ISK Total
        self.sort_column = sort_column

        # Evepraisal API Config
        self.base_url = 'https://evepraisal.com/appraisal/structured.json'
        self.headers = {'User-Agent': ('WIP/%s +https://gitlab.com/eluone/WIP' % version),
                        'Content-Type': 'application/json'}

        # Preferred lowercase market name for API data (amarr, dodixie, hek, jita, perimeter, rens)
        self.market_name = market_name
        # Buy data to use (avg, max, median, min, percentile)
        self.buy_data_field = buy_data_field
        # Sell data to use (avg, max, median, min, percentile)
        self.sell_data_field = sell_data_field
        # Age in seconds to cache data for (1800 = 30 minutes)
        self.cache_max_age = cache_max_age

# end of class Config


current_version = '0.0.96'
config_json = Path('config.json')


def save_config(config):
    # Save the config to file
    with open(config_json, 'w') as f:
        json.dump(config.__dict__, f, indent=4)


def load_config():
    # Lets try to load up previous material selections from the json file.
    if config_json.exists():
        with open(config_json, 'r') as f:
            data = json.load(f)

        if data:
            config = Config(version=data['version'],
                            sql_file=data['sql_file'],
                            missing_mats=data['missing_mats'],
                            min_runs=data['min_runs'],
                            allow_config_override=data['allow_config_override'],
                            # Output List Sorting Column: 0: Blueprint Name, 3: Max Runs Column, 5: ISK Total
                            sort_column=data['sort_column'],
                            market_name=data['market_name'],
                            buy_data_field=data['buy_data_field'],
                            sell_data_field=data['sell_data_field'],
                            cache_max_age=data['cache_max_age']
                            )

        # Check if we're on a new version and update config file.
        if config.version != current_version:
            config.version = current_version
    else:
        # Build the default configuration
        config = Config(version=current_version,
                        sql_file='SDE/sde-subset.sqlite',
                        missing_mats=2,
                        min_runs=0,
                        allow_config_override='N',
                        # Output List Sorting Column: 0: Blueprint Name, 3: Max Runs Column, 5: ISK Total
                        sort_column=5,
                        market_name='jita',
                        buy_data_field='percentile',
                        sell_data_field='percentile',
                        cache_max_age=1800
                        )

    # Save / Update generated file
    save_config(config)

    return config
