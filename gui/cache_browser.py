#!/usr/bin/python
"""Eve Online Salvage Solver"""
# Copyright (C) 2021  Tim Cumming
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Tim Cumming (Elusive One)
# Created: 13/07/21
# Modified: 13/07/21

import wx
import locale
locale.setlocale(locale.LC_ALL, '')


class CacheBrowser(wx.Dialog):
    def __init__(self, *args, **kwds):
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_DIALOG_STYLE
        wx.Dialog.__init__(self, *args, **kwds)
        self.SetTitle("Cache Browser")
        self.SetBackgroundColour(wx.NullColour)  # Use system default colour

        # Font Setup
        main_font = wx.Font(9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, 0, "Sans")

        main_sizer = wx.BoxSizer(wx.VERTICAL)

        self.cache_list_ctrl = wx.ListCtrl(self, wx.ID_ANY, size=(-1, 350), style=wx.LC_HRULES | wx.LC_REPORT | wx.LC_VRULES)
        self.cache_list_ctrl.SetFont(main_font)

        self.cache_list_ctrl.AppendColumn("Type ID", format=wx.LIST_FORMAT_CENTRE, width=75)
        self.cache_list_ctrl.AppendColumn("Type Name", format=wx.LIST_FORMAT_LEFT, width=250)
        self.cache_list_ctrl.AppendColumn("Market", format=wx.LIST_FORMAT_CENTRE, width=75)
        self.cache_list_ctrl.AppendColumn("Buy", format=wx.LIST_FORMAT_RIGHT, width=100)
        self.cache_list_ctrl.AppendColumn("Sell", format=wx.LIST_FORMAT_RIGHT, width=100)
        self.cache_list_ctrl.AppendColumn("Updated", format=wx.LIST_FORMAT_LEFT, width=250)

        main_sizer.Add(self.cache_list_ctrl, 1, wx.EXPAND, 0)

        button_sizer = wx.StdDialogButtonSizer()
        main_sizer.Add(button_sizer, 0, wx.ALIGN_RIGHT | wx.ALL, 4)

        self.button_OK = wx.Button(self, wx.ID_OK, "")
        self.button_OK.SetDefault()
        button_sizer.AddButton(self.button_OK)

        self.button_CANCEL = wx.Button(self, wx.ID_CANCEL, "")
        button_sizer.AddButton(self.button_CANCEL)

        button_sizer.Realize()

        self.SetSizer(main_sizer)
        main_sizer.Fit(self)

        self.SetAffirmativeId(self.button_OK.GetId())
        self.SetEscapeId(self.button_CANCEL.GetId())

        self.Layout()

    def show(self, market_data):
        # Use market_data dictionary and show contents
        self.cache_list_ctrl.DeleteAllItems()  # Clear the list_ctrl to start with

        # Use mats_req_dict data for rows in list_ctrl (convert all to strings)
        for key in market_data:
            self.cache_list_ctrl.Append((str(market_data[key].type_id),
                                         str(market_data[key].type_name),
                                         str(market_data[key].market).title(),
                                         locale.format_string("%0.2f", market_data[key].buy, grouping=True),
                                         locale.format_string("%0.2f", market_data[key].sell, grouping=True),
                                         str(market_data[key].updated)
                                         ))

        self.ShowModal()
