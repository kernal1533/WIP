#!/usr/bin/python
"""Eve Online Salvage Solver"""
# Copyright (C) 2021  Tim Cumming
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Tim Cumming (Elusive One)
# Created: 23/04/21
# Modified: 25/07/21

import wx
import locale

from common.api import api_check

locale.setlocale(locale.LC_ALL, '')


class BlueprintDialog(wx.Dialog):
    def __init__(self, *args, **kwds):
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_DIALOG_STYLE
        wx.Dialog.__init__(self, *args, **kwds)
        self.SetTitle("Blueprint Details")

        main_font = wx.Font(9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, 0, "Sans")

        vert_sizer = wx.BoxSizer(wx.VERTICAL)

        self.bpo_name_label = wx.StaticText(self, wx.ID_ANY, 'Blueprint Name')
        self.bpo_name_label.SetFont(main_font)
        self.bpo_name_label.SetMinSize((450, -1))
        vert_sizer.Add(self.bpo_name_label, 0, wx.ALL | wx.EXPAND, 3)

        self.bill_list_ctrl = wx.ListCtrl(self, wx.ID_ANY, style=wx.LC_HRULES | wx.LC_REPORT | wx.LC_VRULES)
        self.bill_list_ctrl.SetFont(main_font)
        self.bill_list_ctrl.AppendColumn("Input Materials", format=wx.LIST_FORMAT_LEFT, width=300)
        self.bill_list_ctrl.AppendColumn("Required", format=wx.LIST_FORMAT_CENTRE, width=75)
        self.bill_list_ctrl.AppendColumn("Have", format=wx.LIST_FORMAT_CENTRE, width=75)
        self.bill_list_ctrl.AppendColumn("Missing", format=wx.LIST_FORMAT_CENTRE, width=75)
        self.bill_list_ctrl.AppendColumn("Buy (Missing)", format=wx.LIST_FORMAT_RIGHT, width=150)
        self.bill_list_ctrl.AppendColumn("Sell (Required)", format=wx.LIST_FORMAT_RIGHT, width=150)
        vert_sizer.Add(self.bill_list_ctrl, 1, wx.ALL | wx.EXPAND, 3)

        self.item_price_list_ctrl = wx.ListCtrl(self, wx.ID_ANY, style=wx.LC_HRULES | wx.LC_REPORT | wx.LC_VRULES)
        self.item_price_list_ctrl.SetFont(main_font)
        self.item_price_list_ctrl.AppendColumn("Current Market", format=wx.LIST_FORMAT_CENTRE, width=150)
        self.item_price_list_ctrl.AppendColumn("Buy", format=wx.LIST_FORMAT_CENTRE, width=200)
        self.item_price_list_ctrl.AppendColumn("Sell", format=wx.LIST_FORMAT_CENTRE, width=200)
        self.item_price_list_ctrl.AppendColumn("Sell Margin (Have All)", format=wx.LIST_FORMAT_CENTRE, width=200)
        self.item_price_list_ctrl.AppendColumn("Sell Margin (Inc Buy Missing)", format=wx.LIST_FORMAT_CENTRE, width=200)
        vert_sizer.Add(self.item_price_list_ctrl, 1, wx.ALL | wx.EXPAND, 3)

        button_sizer = wx.StdDialogButtonSizer()
        vert_sizer.Add(button_sizer, 0, wx.ALIGN_RIGHT | wx.ALL, 4)

        self.button_OK = wx.Button(self, wx.ID_OK, "")
        self.button_OK.SetDefault()
        button_sizer.AddButton(self.button_OK)

        button_sizer.Realize()

        self.SetSizer(vert_sizer)
        vert_sizer.Fit(self)

        self.SetAffirmativeId(self.button_OK.GetId())

        self.Layout()

    def show(self, config, bpo_id, mat_qty_dict, bpo_dict, market_data):
        # Get dictionary of materials required for selected blueprint

        self.SetTitle(str(bpo_dict[bpo_id].type_name))
        self.bpo_name_label.SetLabel(str(bpo_dict[bpo_id].output_type_name))

        self.item_price_list_ctrl.DeleteAllItems()
        self.bill_list_ctrl.DeleteAllItems()

        # Update the bill_list_ctrl item
        buy_total, sell_total = 0, 0

        # Check for items not in the current market data and try to update
        mat_id_list = []
        for key in bpo_dict[bpo_id].input_materials:
            if key not in market_data:
                mat_id_list.append(int(key))
        if len(mat_id_list) > 0:
            market_data.update(api_check(config, mat_id_list, market_data))

        for key, value in bpo_dict[bpo_id].input_materials.items():
            # Check we have all data for item ID in market_data
            if key in market_data:
                item_name = market_data[key].type_name
                # Use mat_qty_dict to check if user has inputted a required material
                if key in mat_qty_dict:
                    material_qty = mat_qty_dict[key]

                    if material_qty > value:
                        missing_qty = 0
                    else:
                        missing_qty = value - material_qty

                    if missing_qty > 0:
                        buy_missing = locale.format_string("%0.2f", (missing_qty * market_data[key].buy), grouping=True)
                        buy_total = buy_total + (missing_qty * market_data[key].buy)
                    else:
                        buy_missing = "0.00"

                    sell_material = locale.format_string("%0.2f", (value * market_data[key].sell), grouping=True)
                    sell_total = sell_total + (value * market_data[key].sell)

                    self.bill_list_ctrl.Append((str(item_name), str(value), str(material_qty),
                                                str(missing_qty), str(buy_missing), str(sell_material)))
                else:
                    # We haven't got the required material, so we'd have to buy it all
                    sell_material = "0.00"
                    buy_missing = locale.format_string("%0.2f", (value * market_data[key].buy), grouping=True)

                    self.bill_list_ctrl.Append((str(item_name), str(value), '0',
                                                str(value), str(buy_missing), str(sell_material)))
            else:
                self.bill_list_ctrl.Append((str(key), str(value), '0', str(value), "No Data", "No Data"))

        self.bill_list_ctrl.Append(("", "", "", "", "", ""))  # Add a blank line
        self.bill_list_ctrl.Append(("Total", "", "", "",
                                    locale.format_string("%0.2f", buy_total, grouping=True),
                                    locale.format_string("%0.2f", sell_total, grouping=True)))

        # Add chosen market data for output item
        output_item = bpo_dict[bpo_id].output_type_id
        market = market_data[output_item].market
        if market_data[output_item].buy > 0:
            buy_price = locale.format_string("%0.2f", market_data[output_item].buy, grouping=True)
        else:
            buy_price = "No Data"

        if market_data[output_item].sell > 0:
            sell_price = locale.format_string("%0.2f", market_data[output_item].sell, grouping=True)
            sell_margin = locale.format_string("%0.2f", (market_data[output_item].sell - sell_total), grouping=True)
            sell_margin_with_costs = locale.format_string("%0.2f",
                                                          (market_data[output_item].sell - (sell_total + buy_total)),
                                                          grouping=True)
        else:
            sell_price = "No Data"
            sell_margin = "Not Enough Data"
            sell_margin_with_costs = "Not Enough Data"

        self.item_price_list_ctrl.Append((str(market).title(),
                                          str(buy_price),
                                          str(sell_price),
                                          str(sell_margin),
                                          str(sell_margin_with_costs)))

        self.ShowModal()
