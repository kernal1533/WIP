#!/usr/bin/python
"""Eve Online Salvage Solver"""
# Copyright (C) 2021  Tim Cumming
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Tim Cumming (Elusive One)
# Created: 20/04/21
# Modified: 25/07/21

import common.init_common
import unittest

from common.config import load_config

config = load_config()


class GenerateMatDictCheck(unittest.TestCase):
    known_values = {'Alloyed Tritanium Bar': 25595, 'Armor Plates': 25605, 'Artificial Neural Network': 25616,
                    'Broken Drone Transceiver': 25596, 'Burned Logic Circuit': 25600, 'Capacitor Console': 25622,
                    'Charred Micro Circuit': 25599, 'Conductive Polymer': 25604, 'Conductive Thermoplastic': 25623,
                    'Contaminated Lorentz Fluid': 25591, 'Contaminated Nanite Compound': 25590, 'Current Pump': 25611,
                    'Damaged Artificial Neural Network': 25597, 'Defective Current Pump': 25592,
                    'Drone Transceiver': 25615, 'Electromagnetic Metaprismatic Sheeting': 54970,
                    'Enhanced Ward Console': 25625, 'Fried Interface Circuit': 25601,
                    'Hyperbolic Metatransistor Array': 54971, 'Impetus Console': 25621,
                    'Intact Armor Plates': 25624, 'Intact Shield Emitter': 25608, 'Interface Circuit': 25620,
                    'Logic Circuit': 25619, 'Lorentz Fluid': 25610, 'Malfunctioning Shield Emitter': 25589,
                    'Melted Capacitor Console': 25603, 'Micro Circuit': 25618, 'Nanite Compound': 25609,
                    'Power Circuit': 25617, 'Power Conduit': 25613, 'Scorched Telemetry Processor': 25588,
                    'Single-crystal Superalloy I-beam': 25614, 'Smashed Trigger Unit': 25593,
                    'Tangled Power Conduit': 25594, 'Telemetry Processor': 25607, 'Thruster Console': 25602,
                    'Trigger Unit': 25612, 'Tripped Power Circuit': 25598, 'Ultraconducting Ablative Nanocable': 54972,
                    'Ward Console': 25606}

    def test_known_values(self):
        """Test SQL query returns dictionary as above"""
        result = common.init_common.gen_mat_dict(config)
        self.assertEqual(self.known_values, result)


class GenerateBpoDictCheck(unittest.TestCase):
    known_values = {'Atron Blueprint': 955,
                    'Brutix Blueprint': 16230,
                    'Nemesis Blueprint': 11378,
                    'Hyperion Blueprint': 24691}

    bpo_dict = common.init_common.gen_bpo_dict(config)

    def test_known_values_bpo_dict(self):
        """Test SQL query returns dictionary values as above"""
        for name, value in self.known_values.items():
            result = self.bpo_dict[value].type_name
            self.assertEqual(name, result)

    def test_expected_size(self):
        result = 4316
        self.assertEqual(len(self.bpo_dict), result)


if __name__ == '__main__':
    unittest.main()
