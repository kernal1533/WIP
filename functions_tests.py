#!/usr/bin/python
"""Eve Online Salvage Solver"""
# Copyright (C) 2021  Tim Cumming
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Tim Cumming (Elusive One)
# Created: 09/05/21
# Modified: 31/07/21

import common.functions
import unittest

from common.classes import Blueprint
from common.config import load_config

config = load_config()


class GetMatchesCheck(unittest.TestCase):
    # Material IDs List vs len of data returned
    known_values = [[[25605, 25590, 25601], 304]]

    def test_known_values(self):
        """Test SQL query returns data of correct length as above"""
        for case in range(len(self.known_values)):
            result = len(common.functions.get_matches(config, self.known_values[case][0]))
            self.assertEqual(self.known_values[case][1], result)


class BpoMatsRequiredDictCheck(unittest.TestCase):
    # Setup a test bpo_dict with some initial date for the function to work with
    bpo_dict = {955: Blueprint(955, 'Atron Blueprint', 0, '', {}),
                16230: Blueprint(16230, 'Brutix Blueprint', 0, '', {}),
                11378: Blueprint(11378, 'Nemesis Blueprint', 0, '', {}),
                24691: Blueprint(24691, 'Hyperion Blueprint', 0, '', {})}

    known_values = [[955, {34: 20556, 35: 3889, 36: 2222, 37: 278, 38: 56, 39: 12}],
                    [16230, {34: 3111111, 35: 700000, 36: 244444, 38: 12778, 39: 4888, 40: 2888}],
                    [11378, {593: 1, 3828: 30, 11399: 38, 11478: 3, 11531: 30, 11535: 60,
                             11541: 180, 11545: 300, 11547: 6, 11553: 60, 11556: 15}],
                    [24691, {34: 13130556, 35: 3282889, 36: 789333, 37: 204967,
                             38: 51267, 39: 25200, 40: 6512}]]

    def test_known_values(self):
        """Test SQL query returns dictionary values as above"""
        for case in range(len(self.known_values)):
            common.functions.bpo_mats_required_dict(config, self.known_values[case][0], self.bpo_dict)
            self.assertEqual(self.known_values[case][1], self.bpo_dict[(self.known_values[case][0])].input_materials)


class BpoCheckMaterialsCheck(unittest.TestCase):
    # Setup a test bpo_dict with some initial date for the function to work with
    bpo_dict = {955: Blueprint(955, 'Atron Blueprint', 0, '', {}),
                16230: Blueprint(16230, 'Brutix Blueprint', 0, '', {}),
                11378: Blueprint(11378, 'Nemesis Blueprint', 0, '', {}),
                24691: Blueprint(24691, 'Hyperion Blueprint', 0, '', {})}

    # bpo_id, Correct Material Quantities, Number of materials, 1 run
    known_values = [[955, {34: 20556, 35: 3889, 36: 2222, 37: 278, 38: 56, 39: 12},
                     (6, 1)],
                    [16230, {34: 3111111, 35: 700000, 36: 244444, 38: 12778, 39: 4888, 40: 2888},
                     (6, 1)],
                    [11378, {593: 1, 3828: 30, 11399: 38, 11478: 3, 11531: 30, 11535: 60,
                             11541: 180, 11545: 300, 11547: 6, 11553: 60, 11556: 15},
                     (11, 1)],
                    [24691, {34: 13130556, 35: 3282889, 36: 789333, 37: 204967,
                             38: 51267, 39: 25200, 40: 6512},
                     (7, 1)]]

    def test_known_values(self):
        """Test number of runs calculator works with materials required dictionary values as above"""
        for case in range(len(self.known_values)):
            result = common.functions.bpo_check_materials(config, self.known_values[case][0],
                                                          self.known_values[case][1], self.bpo_dict)
            self.assertEqual(self.known_values[case][2], result)


class AddMarketDataCheck(unittest.TestCase):
    # add_market_data(input_list, bpo_dict, market_data)
    pass


class SortForOutputCheck(unittest.TestCase):
    # Use sort_for_output with out_list sorting on first column (name)
    out_list = [('Small Remote Repair Augmentor I Blueprint', '3/3', '3/3', 1),
                ('Small Trimark Armor Pump I Blueprint', '3/3', '3/3', 1),
                ('Small EM Armor Reinforcer I Blueprint', '3/3', '3/3', 1),
                ('Small Explosive Armor Reinforcer I Blueprint', '3/3', '3/3', 1),
                ('Small Kinetic Armor Reinforcer I Blueprint', '3/3', '3/3', 1),
                ('Small Thermal Armor Reinforcer I Blueprint', '3/3', '3/3', 1)]
    sorted_out_list = [('Small Trimark Armor Pump I Blueprint', '3/3', '3/3', 1),
                       ('Small Thermal Armor Reinforcer I Blueprint', '3/3', '3/3', 1),
                       ('Small Remote Repair Augmentor I Blueprint', '3/3', '3/3', 1),
                       ('Small Kinetic Armor Reinforcer I Blueprint', '3/3', '3/3', 1),
                       ('Small Explosive Armor Reinforcer I Blueprint', '3/3', '3/3', 1),
                       ('Small EM Armor Reinforcer I Blueprint', '3/3', '3/3', 1)]

    def test_known_values(self):
        result = common.functions.sort_for_output(self.out_list, 0)
        self.assertEqual(self.sorted_out_list, result)


class UnfilteredBpoListCheck(unittest.TestCase):
    # Setup a test bpo_dict with some initial date for the function to work with
    bpo_dict = {955: Blueprint(955, 'Atron Blueprint', 0, '', {}),
                16230: Blueprint(16230, 'Brutix Blueprint', 0, '', {}),
                11378: Blueprint(11378, 'Nemesis Blueprint', 0, '', {}),
                24691: Blueprint(24691, 'Hyperion Blueprint', 0, '', {})}

    # unfiltered_bpo_list(config, match_list, mat_qty_dict, bpo_dict)
    pass


class ApplyUserFiltersCheck(unittest.TestCase):
    # Setup a test bpo_dict with some initial date for the function to work with
    bpo_dict = {955: Blueprint(955, 'Atron Blueprint', 0, '',
                               {34: 20556, 35: 3889, 36: 2222, 37: 278, 38: 56, 39: 12}),
                16230: Blueprint(16230, 'Brutix Blueprint', 0, '',
                                 {34: 3111111, 35: 700000, 36: 244444, 38: 12778, 39: 4888, 40: 2888}),
                11378: Blueprint(11378, 'Nemesis Blueprint', 0, '',
                                 {593: 1, 3828: 30, 11399: 38, 11478: 3, 11531: 30, 11535: 60,
                                  11541: 180, 11545: 300, 11547: 6, 11553: 60, 11556: 15}),
                24691: Blueprint(24691, 'Hyperion Blueprint', 0, '',
                                 {34: 13130556, 35: 3282889, 36: 789333, 37: 204967,
                                  38: 51267, 39: 25200, 40: 6512}
                                 )}
    min_runs = 1
    num_missing_mats = 0
    allow_config_override = 'N'

    # apply_user_filters(input_list, bpo_dict, min_runs, num_missing_mats, allow_config_override)
    pass


if __name__ == '__main__':
    unittest.main()
